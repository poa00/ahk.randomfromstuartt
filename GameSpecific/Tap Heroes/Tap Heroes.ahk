﻿r::
    SetDefaultMouseSpeed, 0
    CoordMode, Mouse, Client
    stopLoop := false
    WinGet, gameWinID, ID, ahk_exe Tap Heroes.exe
    WinActivate, ahk_id %gameWinID%
    Loop {
        if stopLoop
            break
        MouseMove 350, 250 ; heal
	Click down
	Sleep, 50
	Click up
        MouseMove 400, 250 ; attack
	Click down
	Sleep, 50
	Click up
    }
    return

e::
    stopLoop := false
    Loop {
        if stopLoop
            break
        SendEvent {Click}
    }
    return

t::
    stopLoop := true
    return

k::
    SetDefaultMouseSpeed, 0
    CoordMode, Mouse, Client
    stopLoop := false
    WinGet, gameWinID, ID, ahk_exe Tap Heroes.exe
    WinActivate, ahk_id %gameWinID%
    Loop {
        if stopLoop
            break
        SendEvent {Click 77, 340} ; Hand Of God
        SendEvent {Click 126, 340} ; Gold Rush
        SendEvent {Click 180, 340} ; Great Tide
        SendEvent {Click 455, 340} ; Battle Cry
        SendEvent {Click 504, 340} ; Healing Light
        SendEvent {Click 555, 340} ; Arrow to the knee
    }
    return

z::
    CoordMode, Pixel, Client
    CoordMode, Mouse, Client

    stopLoop := false
    Loop {
        if stopLoop
            break

        ; Run the game if not exist
        IfWinNotExist, ahk_exe Tap Heroes.exe
            ; Change path here to game executable on your system
            Run, E:\Games\Steam\steamapps\common\Tap Heroes\Tap Heroes.exe

        ; Wait for the game window and skip welcome messages
        WinWait, ahk_exe Tap Heroes.exe
        WinGet, gameWinID, ID, ahk_exe Tap Heroes.exe
	Sleep, 3000
        WinActivate, ahk_id %gameWinID%
        Loop {
            if stopLoop
                break
            SendEvent {Click 325, 250}
            MouseMove, 20, 20
            PixelGetColor, PixCol, 20, 20
            IfEqual, PixCol, 0x56C7E1
                break
        }
        Sleep, 1000

        ; Try to go forward if level 299 and backward if level 300
        ; Actually checks for the colour in the middle of the last number of the level
        PixelGetColor, PixCol, 367, 51
        IfNotEqual, PixCol, 0x000000
            Loop {
                if stopLoop
                    break
		Sleep, 200
                SendEvent {Click 535, 195}
                Sleep, 1000
                PixelGetColor, PixCol, 367, 51
                IfEqual, PixCol, 0x000000
                    break
            }
            Loop {
                if stopLoop
                    break
		Sleep, 200
                SendEvent {Click 95, 195}
                Sleep, 1000
                PixelGetColor, PixCol, 367, 51
                IfNotEqual, PixCol, 0x000000
                    break
            }

        ; Now exit
        WinClose, ahk_id %gameWinID%
        Sleep, 100
        Send {enter}
    }
    return