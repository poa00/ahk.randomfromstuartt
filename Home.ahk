;------------------------------------------------------------------------------
; Settings
;------------------------------------------------------------------------------

#InstallKeybdHook
#UseHook, On
#WinActivateForce
#SingleInstance force
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
SetTitleMatchMode, RegEx
DetectHiddenWindows, On
SetCapsLockState, AlwaysOff

Menu, Tray, Icon, lib\images\Portal.ico
Menu, Tray, Tip, Home Script

;------------------------------------------------------------------------------
; Includes
;------------------------------------------------------------------------------

#Include *i %A_ScriptDir%\Core\Functions.ahk
#Include %A_ScriptDir%\Core\GlobalVars.ahk
#Include %A_ScriptDir%\Util\Init.ahk
#Include %A_ScriptDir%\Util\ParseSettings.ahk
#Include %A_ScriptDir%\Util\TrayMenu.ahk

;------------------------------------------------------------------------------
; Run at start Scripts
;------------------------------------------------------------------------------

If( Settings.StartupNotification )
  Notify( Settings.ScriptName " Started", , -3, "Style=Win10" )

If( Settings.UseAutoCorrect )
  Run, %A_ScriptDir%\Util\AutoCorrect.ahk
Else {
  DetectHiddenWindows, On
  WinClose, %A_ScriptDir%\Util\AutoCorrect.ahk ahk_class AutoHotkey
}

If( Settings.UseGlossary )
  Run, %A_ScriptDir%\Util\Glossary.ahk
Else {
  DetectHiddenWindows, On
  WinClose, %A_ScriptDir%\Util\Glossary.ahk ahk_class AutoHotkey
}

;------------------------------------------------------------------------------
; Other
;------------------------------------------------------------------------------

CaseToTitle() {
  convertCaseTo("Title")
  Return
}

CaseToUpper() {
  convertCaseTo("Upper")
  Return
}

CaseToLower() {
  convertCaseTo("Lower")
  Return
}

winMin() {
  WinMinimize, A
  Return
}

;------------------------------------------------------------------------------
; Open Directories
;------------------------------------------------------------------------------

openDownloadDir() {
  openOrCloseDir(downloadWName, downloadPath)
  Return
}

openDocumentDir() {
  openOrCloseDir(mydocWName, A_MyDocuments)
  Return
}

openPicturesDir() {
  openOrCloseDir(mypicturesWName, mypicturePath)
  Return
}

openVideosDir() {
  openOrCloseDir(myvideosWName, myvideosPath)
  Return
}

;openDropBox() {
;  openOrCloseDir(dropWName, dropPath)
;  Return
;}

;------------------------------------------------------------------------------
; Other Includes
;------------------------------------------------------------------------------

#Include *i %A_ScriptDir%\Core\Hotkeys.ahk
#Include *i %A_ScriptDir%\Core\AppSpecific.ahk
#Include *i %A_ScriptDir%\Core\HotStrings.ahk
