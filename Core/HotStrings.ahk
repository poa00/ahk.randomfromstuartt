/*
HotString Documentation: http://ahkscript.org/docs/Hotstrings.htm

Examples:

  Single Line
  ::btw::by the way

  Multi-Line
  ::btw::
  MsgBox You typed "btw".
Return
*/

;------------------------------------------------------------------------------
; HOTSTRING DATES
;------------------------------------------------------------------------------

; type "mysqldate" to enter current date in year-day-month format | Ex. 2015-02-26
::mysqldate::
{
  FormatTime, CurrentDateTime,, yyyy-MM-dd
  SendInput %CurrentDateTime%
  Return
}

; type "cdate" to enter current date in month/day/year format | Ex. 02/14/2014
::cdate::
{
  FormatTime, CurrentDateTime,, MM/d/yyyy
  SendInput %CurrentDateTime%
  Return
}

; type "cddate" to enter current date in month-day-year format | Ex. 02-14-2014
::cddate::
{
  FormatTime, CurrentDateTime,, MM-d-yyyy
  SendInput %CurrentDateTime%
  Return
}

; type "fdate" to enter current date in dayofweek, month day, year | Ex. Friday, February 14, 2014
::fdate::
{
  FormatTime, CurrentDateTime,, dddd, MMMM d, yyyy
  SendInput %CurrentDateTime%
  Return
}

; type "thedate" to enter the current date in month, year | Ex. April 2014
::thedate::
{
  FormatTime, CurrentDateTime,, MMMM yyyy
  SendInput %CurrentDateTime%
  Return
}

; type "sdate" to enter the current date in month-day-year | Ex. Feb-18-2014
::sdate::
{
  FormatTime, CurrentDateTime,, MMM-dd-yyyy
  SendInput %CurrentDateTime%
  Return
}

; Enwin date | Ex. Sep 29/14
::edate::
{
  FormatTime, CurrentDateTime,, MMM dd/y
  SendInput %CurrentDateTime%
  Return
}

; Enwin update date | Ex. 111814
::enwindate::
{
  FormatTime, CurrentDateTime,, MMddyyyy
  SendInput %CurrentDateTime%
  Return
}

;-------------------------------------------------------------------------------
; RANDOM HOTSTRINGS/HOTKEYS
;-------------------------------------------------------------------------------

::xrefresh::You may need to refresh to see the changes.
::xtest::This is a test. Please ignore.
::xdesc::description
::xcr::chrome://restart

;-------------------------------------------------------------------------------
; CODE
;-------------------------------------------------------------------------------

::xwpackagist::composer require wpackagist-plugin/plugin:dev-trunk
::xatom::atom --disable-direct-write
::xcopy::©
::xreg::®
::x404::ErrorDocument 404 /404.php
::xr301::Redirect 301 /
;::xdrarrow::»
;::xdlarrow::«
;::xrarrow::→
;::xlarrow::←
::xbr::<br />
::xhr::<hr />
::xbsp:: 
::xlorem::Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lorem augue, egestas scelerisque imperdiet semper, imperdiet eu diam. Pellentesque vulputate nunc eget est malesuada eleifend. Sed mollis convallis tempor. Nulla sed convallis eros. Proin lectus ante, commodo ac euismod at, dignissim eget orci. Suspendisse molestie magna laoreet tellus condimentum tempus. In adipiscing suscipit lobortis.
::xview::<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
::xie::<meta http-equiv="x-ua-compatible" content="ie=edge">
